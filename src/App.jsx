import React, { Component } from 'react';
import Countdown from './Countdown.jsx';
//import Marquee from 'marquee-react-dwyer';


class App extends Component {

  state = { 
    date: '2018-08-19',
    time: 0,
    message: '',
    orangeAlert: 0,
    redAlert: 0
  }

  handleTime = (x) => {
    this.setState({ time: x })
  }

  handleMessage = (x) => {
    this.setState({ message: x })
  }

  handleOrangeAlert = (x) => {
    this.setState({ orangeAlert: x })
  }

  handleRedAlert = (x) => {
    this.setState({ redAlert: x })
  }

  async getFreshData() {

    var myHeaders = new Headers();
    myHeaders.append('pragma', 'no-cache');
    myHeaders.append('cache-control', 'no-cache');

    var myInit = {
      method: 'GET',
      headers: myHeaders,
      mode: "cors",
      cache: "no-store",
    };

    fetch('https://odpocitavadlo.mksnj.cz//wp-json/acf/v2/options', { method: "GET", myInit /*mode: "cors", cache: "no-store" */})
      .then((response) => response.json())
      .then((responseData) => {
        //set your data here
        this.handleTime(responseData.acf.cas);
        this.handleMessage(responseData.acf.zprava);
        this.handleOrangeAlert(responseData.acf.nastaveni_prvniho_upozorneni);
        this.handleRedAlert(responseData.acf.nastaveni_hlavniho_upozorneni);
        console.log(responseData.acf.cas);
        console.log(responseData.acf.zprava);
      })
      .catch((error) => {
        console.error(error);
      });

  }

  componentDidMount() {  
    this.getFreshData();
    
    this.timer = setInterval(() => this.getFreshData(), 10000);
  }


  

  render() {        
    //console.log(this.state);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }

    today = yyyy+'-'+mm+'-'+dd;   

    return (      
      <div className="App">      
        <Countdown date={`${today}T${this.state.time}`} alert={this.state.orangeAlert} />              
        
        <marquee>{this.state.message}</marquee>
       
      </div>
    );
  }
}

export default App;
